create table customer(
client_id int primary key generated always as identity,
first_name varchar(200) not null,
last_name varchar(200)
);

drop table customer;
drop table account;

delete table customer;
delete table account;

create table account(
account_id int primary key generated always as identity,
account_type varchar(200),
account_balance int,
existing_account boolean,
c_id int, --will hold what client ID this account has
constraint fk_account_customer foreign key (c_id) references customer(client_id)
);

select * from customer;
select * from customer where first_name like'J%';
select * from account;

