import { convertToObject } from "typescript";
import { client } from "../src/connection";
import { CustomerDAO } from "../src/src/dao/bank-dao";
import { CustomerDaoPostgres } from "../src/src/dao/bank-dao-postgres";
import { CustomerDaoTextFile } from "../src/src/dao/bank-dao-impl";
import { Account, Customer } from "../src/src/entities";
import { BankServiceImpl } from "../src/src/services/bank-service-impl";


const customerDAO:CustomerDAO = new CustomerDaoPostgres();
const testCustomer:Customer = new Customer(0, "Rafael", "Mora");

test("Create a Customer", async ()=>{
    const result:Customer = await customerDAO.createCustomer(testCustomer);
    console.log(result);
    expect(result.clientId).not.toBe(0);
});

test("Get Customer by Id", async ()=>{
   let customer:Customer = new Customer(0, "Rafael", "Mora");
   customer = await customerDAO.createCustomer(customer);
   let retreivedCustomer:Customer = await customerDAO.getCustomerById(customer.clientId);
    expect(retreivedCustomer.clientId).toBe(customer.clientId);
});


test("Get all customers", async ()=>{
    let customer1:Customer = new Customer(0, "Rafael", "Alexander");
    let customer2:Customer = new Customer(0, "Nicholas", "Michael");
    let customer3:Customer = new Customer(0, "Natalie", "Elizabeth");
    await customerDAO.createCustomer(customer1);
    await customerDAO.createCustomer(customer2);
    await customerDAO.createCustomer(customer3);

    const customers:Customer[] = await customerDAO.getAllCustomers();
    expect(customers.length).toBeGreaterThanOrEqual(0);
})

test("Update Customer", async() =>{
    let customer:Customer = new Customer(0,"Lionel", "Messi");
    customer = await customerDAO.createCustomer(customer);

    customer.clientId = 0;
    customer = await customerDAO.updateCustomer(customer);

    expect(customer.clientId).toBe(0);
})

test("Delete Customer by Id", async()=>{
    let customer:Customer = new Customer(0, "Cristiano", "Ronaldo");
    customer = await customerDAO.createCustomer(customer);

    const result:boolean = await customerDAO.deleteCustomerbyId(customer.clientId);
    expect(result).toBeTruthy();
})


afterAll(()=>{
    client.end();
});