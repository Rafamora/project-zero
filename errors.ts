
export class MissingResourceError{

    message:string;
    description:string = "This error means a customer or account cannot be found";

    constructor(message:string){
        this.message = message;

    }
}