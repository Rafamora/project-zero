import { Account, Customer } from "../entities";

export default interface BankService{

registerCustomer(customer:Customer):Promise<Customer>;

retreiveAllCustomers():Promise<Customer[]>;

retrieveCustomerById(clientId:number):Promise<Customer>;

openAccount(accountId:number):Promise<Customer>;

closeAccount(accountId:number):Promise<Account>;

depositIntoAccount(accountId:number):Promise<Account>;

withdrawFromAccount(accountId:number):Promise<Account>;

removeCustomerById(clientId:number):Promise<Customer>;

}