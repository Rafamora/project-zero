import { CustomerDAO } from "../dao/bank-dao";
import { CustomerDaoPostgres } from "../dao/bank-dao-postgres";
import { CustomerDaoTextFile } from "../dao/bank-dao-impl";
import { Account, Customer } from "../entities";
import BankService from "./bank-service";

export class BankServiceImpl implements BankService{

    customerDao:CustomerDAO = new CustomerDaoPostgres();
    
    registerCustomer(customer: Customer): Promise<Customer> {
        return this.customerDao.createCustomer(customer); //DONE--app.post
    }

    retreiveAllCustomers(): Promise<Customer[]> {
        return this.customerDao.getAllCustomers(); //DONE--app.get
    }

    retrieveCustomerById(clientId: number): Promise<Customer> {
        return this.customerDao.getCustomerById(clientId); //DONE--app.get
    }

    openAccount(accountId: number): Promise<Customer> {
        throw new Error("Method not implemented.");
    }

    closeAccount(accountId: number): Promise<Account> {
        throw new Error("Method not implemented.");
    }

    depositIntoAccount(accountId: number): Promise<Account> {
        throw new Error("Method not implemented.");
    }

    withdrawFromAccount(accountId: number): Promise<Account> {
        throw new Error("Method not implemented.");
    }


    removeCustomerById(clientId: number): Promise<Customer> {
        throw new Error("Method not implemented.");
    }
   
}