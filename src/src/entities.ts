
export class Customer{
    constructor(
        public clientId:number,
        public firstName:string,
        public lastName:string,
    ){}
    }

    export class Account{
        constructor(
        public clientId:number,
        public accountId:number,
        public accountType:string,
        public accountBalance:number,
        public existingAccount: boolean
    ){}
    }

    //will have one book-dao per entity listed above
    //Did not want to define an account type specific to checking, savings,etc. because it mentioned there being a variety of types of accounts that could be made
    //Two separate classes created