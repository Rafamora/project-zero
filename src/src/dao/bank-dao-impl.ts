import { Customer, Account } from "../entities";
import { CustomerDAO } from "./bank-dao";
import { readFile, writeFile } from "fs/promises";
import { MissingResourceError } from "../../../errors";


export class CustomerDaoTextFile implements CustomerDAO{
    
    async createCustomer(customer: Customer): Promise<Customer> {
        const fileData:Buffer = await readFile('/Users/rafael.mora/Desktop/BankingAppAPI/bank.txt');
        const textData:string = fileData.toString();
        const customers:Customer[] = JSON.parse(textData);
        customer.clientId = Math.round(Math.random()*100);
        customers.push(customer);
        await writeFile('/Users/rafael.mora/Desktop/BankingAppAPI/bank.txt', JSON.stringify(customers));
        return customer;
    }

    async getAllCustomers(): Promise<Customer[]> {
        const fileData:Buffer = await readFile('/Users/rafael.mora/Desktop/BankingAppAPI/bank.txt');
        const textData:string = fileData.toString();
        const customers:Customer[] = JSON.parse(textData);
        return customers;
    }

    async getCustomerById(clientId: number): Promise<Customer> {
        console.log("Calling impl getCustomerBYid");
        const fileData:Buffer = await readFile('/Users/rafael.mora/Desktop/BankingAppAPI/bank.txt');
        const textData:string = fileData.toString();
        const customers:Customer[] = JSON.parse(textData);
        
        for (const customer of customers){
            if(customer.clientId === clientId){
                return customer;
            }
        }
        throw new MissingResourceError(`The customer or account with id ${clientId} could not be located`);
    }

     getAllAccounts(): Promise<Account[]> {
        throw new Error("Method not implemented.");
    }

    async getAccountById(accountId: number): Promise<Account> {
        const fileData:Buffer = await readFile('/Users/rafael.mora/Desktop/BankingAppAPI/bank.txt');
        const textData:string = fileData.toString();
        const customer:Customer[] = JSON.parse(textData)
        throw new Error("Method not implemented.");
    }

    async updateCustomer(customer: Customer): Promise<Customer> {
        const fileData:Buffer = await readFile('/Users/rafael.mora/Desktop/BankingAppAPI/bank.txt');
        const textData:string = fileData.toString();
        const customers:Customer[] = JSON.parse(textData);
        
        for(let i =0; i< customers.length; i++){
            if(customers[i].clientId === customer.clientId){
                customers[i] = customer;
            }
        }
        
        await writeFile('/Users/rafael.mora/Desktop/BankingAppAPI/bank.txt', JSON.stringify(customers));
        return customer;
    }

    async deleteCustomerbyId(clientId: number): Promise<boolean> {
        const fileData:Buffer = await readFile('/Users/rafael.mora/Desktop/BankingAppAPI/bank.txt');
        const textData:string = fileData.toString();
        const customers:Customer[] = JSON.parse(textData);
        
        for(let i =0; i< customers.length; i++){
            if(customers[i].clientId === clientId){
                customers.splice(i)
                await writeFile('/Users/rafael.mora/Desktop/BankingAppAPI/bank.txt', JSON.stringify(customers));
                return true;
            }
        }
        
        return false;
    }
}