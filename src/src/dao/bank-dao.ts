import { Account, Customer } from "../entities";

export interface CustomerDAO{

//CREATE
    createCustomer(customer:Customer):Promise<Customer>; //PASSED 

//READ
    getAllCustomers():Promise<Customer[]>; //PASSED
    getCustomerById(clientId:number):Promise<Customer>; //PASSED
    getAllAccounts():Promise<Account[]>;
    getAccountById(accountId:number):Promise<Account>;

//UPDATE
    updateCustomer(customer:Customer):Promise<Customer>; //PASSED

//DELETE
    deleteCustomerbyId(clientId:number):Promise<boolean>; //PASSED
}