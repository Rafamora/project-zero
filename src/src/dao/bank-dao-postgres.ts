import { Customer, Account } from "../entities";
import { CustomerDAO } from "./bank-dao";
import {client } from "../../connection";

export class CustomerDaoPostgres implements CustomerDAO{
    
    async createCustomer(customer: Customer): Promise<Customer> {
        const sql:string = "insert into customer(first_name,last_name) values ($1,$2) returning client_id";
        const values = [customer.firstName,customer.lastName];
        const result = await client.query(sql,values);
        customer.clientId = result.rows[0].client_id;
        console.log(result.rows[0].client_id);
        console.log(customer.clientId);
        return customer;

    }
    async getAllCustomers(): Promise<Customer[]> {
        const sql:string = 'select * from customer';
        const result = await client.query(sql);
        const customers:Customer[] = [];
        for(const row of result.rows){
            const customer:Customer = new Customer(
                row.client_id,
                row.first_name,
                row.last_name);
            customers.push(customer);
        }
        return customers;
    }
    
    async getCustomerById(clientId: number): Promise<Customer> {
        const sql:string = 'select * from customer where client_id = $1';
        const values = [clientId];
        const result = await client.query(sql,values);
        const row = result.rows[0];
        const customer:Customer = new Customer(
            row.client_id,
            row.first_name,
            row.last_name);
        return customer;
    }
    
    async getAllAccounts(): Promise<Account[]> {
        throw new Error("Method not implemented.");
    }
    async getAccountById(accountId: number): Promise<Account> {
        throw new Error("Method not implemented.");
    }

    updateCustomer(customer: Customer): Promise<Customer> {
        throw new Error("Method not implemented.");
    }
    deleteCustomerbyId(clientId: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }



    
}