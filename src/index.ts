import express from 'express';
import { MissingResourceError } from '../errors';
import { Customer } from './src/entities';
import BankService from './src/services/bank-service';
import { BankServiceImpl } from './src/services/bank-service-impl';

//THIS IS THE API Layer--uses the service layer methods--relies on services to complete operations.

const app = express();
app.use(express.json()); //Middleware

const bankService:BankService = new BankServiceImpl();

app.post("/customers", async (req, res) => {
    let customer:Customer = req.body;
    console.log("Create Customer");
    customer = await bankService.registerCustomer(customer);
    res.send(customer);
});

app.get("/customers", async (req, res) => {
        const customers:Customer[] = await bankService.retreiveAllCustomers();
        res.send(customers);   
    });

app.get("/customers/:id", async (req, res) =>{
    try {
        const clientId = Number(req.params.id);
        const customer:Customer = await bankService.retrieveCustomerById(clientId);
        res.send(customer);  
    } catch (error) {
       if(error instanceof MissingResourceError){
           res.status(404);
           res.send(error);
       } 
    }
    
});

app.listen(3005,()=>{console.log("Application Started")})